void main() {
  List<String> mtnappoftheyear = [
    //Listing the apps starting at 2012: FNB Banking
    'FNB Banking',
    'SnapScan',
    'LIVE inspect',
    'WumDrop',
    'Domestly',
    'Shyft',
    'Agricultural app Khula',
    'Naked Insurance',
    'EasyEquities',
    'Ambani Africa',
  ];
  List<int> years = [
    2012,
    2013,
    2014,
    2015,
    2016,
    2017,
    2018,
    2019,
    2020,
    2021
  ];

  Map<int, String> appPerYear = {};
  for (var i = 0; i < mtnappoftheyear.length; i++) {
    appPerYear[years[i]] = mtnappoftheyear[i];
  }

  print("The MTN app of the year for 2017 is: ${appPerYear[2017]}");
  print(
      "The MTN app of the year for 2018 is: ${appPerYear[2018]}"); //I know a more elegant solution exists where I can print the 2017 and 2018 apps but could not understand it yet.
  mtnappoftheyear.sort((a, b) {
    //Putting the list of apps in alphabetical order
    return a.compareTo(b);
  });
  print(
      "The MTN app of the year winners in alphebetical order are:  $mtnappoftheyear");
  print("The number of apps in the array: ${mtnappoftheyear.length}");
}
