// Defining class
// ignore_for_file: unnecessary_new, prefer_typing_uninitialized_variables

class App {
  var app;
  var sector;
  var developer;
  var year;

  // defining class function

  showAppInfo() {
    print("MTN app of the year is : $app");
    print("The sector it operates in : $sector");
    print("The developer is: $developer");
    print("The year it won the award : $year");
    print("                                   ");
  }
}

void main() {
  // Creating object called a1

  App a1 = new App();
  a1.app = "FNB Banking".toUpperCase();
  a1.sector = "Banking";
  a1.developer = "FNB";
  a1.year = 2012;
  // Creating object called a2
  App a2 = new App();
  a2.app = "SnapScan".toUpperCase();
  a2.sector = "Mobile Payments";
  a2.developer = "SnapScan";
  a2.year = 2013;
  // Creating object called a3
  App a3 = new App();
  a3.app = "LIVE inspect".toUpperCase();
  a3.sector = "Insurance";
  a3.developer = "LIVE inspect";
  a3.year = 2014;
  // Creating object called a4
  App a4 = new App();
  a4.app = "WumDrop".toUpperCase();
  a4.sector = "Logistics";
  a4.developer = "WumDrop";
  a4.year = 2015;
  // Creating object called a5
  App a5 = new App();
  a5.app = "Domestly".toUpperCase();
  a5.sector = "Cleaning Services";
  a5.developer = "Domestly";
  a5.year = 2016;
  // Creating object called a6
  App a6 = new App();
  a6.app = "Shyft".toUpperCase();
  a6.sector = "Financial Services";
  a6.developer = "Standard Bank";
  a6.year = 2017;
  // Creating object called a7
  App a7 = new App();
  a7.app = "Khula Ecosystem".toUpperCase();
  a7.sector = "Agriculture";
  a7.developer = "Khula";
  a7.year = 2018;
  // Creating object called a8
  App a8 = new App();
  a8.app = "Naked Insurance".toUpperCase();
  a8.sector = "Insurance";
  a8.developer = "Naked Insurance";
  a8.year = 2019;
  // Creating object called a9
  App a9 = new App();
  a9.app = "Easy Equities".toUpperCase();
  a9.sector = "Investment";
  a9.developer = "First World Trader Pty";
  a9.year = 2020;
  // Creating object called a10
  App a10 = new App();
  a10.app = "Ambani Africa".toUpperCase();
  a10.sector = "Language Services";
  a10.developer = "Ambani Africa";
  a10.year = 2021;

// Accessing class Function
  a1.showAppInfo();
  a2.showAppInfo();
  a3.showAppInfo();
  a4.showAppInfo();
  a5.showAppInfo();
  a6.showAppInfo();
  a7.showAppInfo();
  a8.showAppInfo();
  a9.showAppInfo();
  a10.showAppInfo();
}
